# Python speedtest.net server and client

A simple asynchronous implementation of the speedtest server and client.

**Requires python 3.6 or higher**

## Usage

```bash
# Server
python server.py <port>

# Client
python client.py <hostname> <port>
```