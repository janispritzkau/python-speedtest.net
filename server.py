from time import time
import asyncio
import sys
import os

class Server:
    random_bytes = os.urandom(1_000_000)

    @staticmethod
    async def bind(host, port):
        server = Server()
        await asyncio.start_server(server.handle_client, host, port)
        return server

    async def handle_client(self, reader, writer):
        print("Client connected")
        try:
            while not reader.at_eof():
                line = await reader.readline()
                if len(line) <= 1 or len(line) > 40:
                    return writer.close()
                command = line.decode()[:-1].split(' ')

                print(command)
                should_return = await self.run_command(reader, writer, command, len(line))
                await writer.drain()

                if should_return:
                    return writer.close()
        except Exception as err:
            raise err

    async def run_command(self, reader, writer, command, command_len):
        async def hi():
            writer.write("HELLO python-speedtest.net\n".encode())

        async def ping():
            writer.write("PONG {}\n".format(command[1]).encode())

        async def download():
            bytes = int(command[1])
            if bytes > 10_000_000:
                return True
            missing = bytes - 1
            while missing > 0:
                buf = self.random_bytes[:min(missing, len(self.random_bytes))]
                missing -= len(buf)
                writer.write(buf)
            writer.write(b"\n")

        async def upload():
            bytes = int(command[1]) - command_len
            if bytes > 10_000_000:
                return True
            buf = await reader.readexactly(bytes)
            writer.write("OK {} {}\n".format(bytes + command_len, round(time())).encode())

        commands = {
            'HI': hi,
            'PING': ping,
            'DOWNLOAD': download,
            'UPLOAD': upload
        }
        return await commands.get(command[0])()

async def main():
    if len(sys.argv) < 2:
        print("Usage: python server.py <port>")
        exit()
    port = int(sys.argv[1])

    await Server.bind("0.0.0.0", port)

if __name__ == "__main__":
    try:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(main())
        loop.run_forever()
    except KeyboardInterrupt:
        print("Server stopeed")
