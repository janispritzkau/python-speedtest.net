from math import floor
import asyncio
import time
import sys
import os

class Client:
    @staticmethod
    async def connect(host, port):
        """Open a new connection to a server"""
        reader, writer = await asyncio.open_connection(host, port)
        client = Client()
        (client.reader, client.writer) = (reader, writer)
        # genereate some random bytes to be used for testing upload speed
        client.random_bytes = os.urandom(500_000)
        return client

    async def hi(self):
        self.writer.write(b"HI\n")
        return (await self.reader.readline()).decode()[:-1]

    async def ping(self):
        """Measure the latency of the connection in milliseconds"""
        start = time.time()
        self.writer.write(b"PING %d\n" % floor(start * 1000))
        await self.writer.drain()
        start = time.time()
        reply = await self.reader.readline()
        elapsed = time.time() - start
        if not reply.decode().startswith("PONG "):
            raise Exception("Bad reply")
        return elapsed * 1000

    async def download(self, bytes):
        """Measure the time it takes to download the specified amount of bytes and return the speed in bytes/s"""
        self.writer.write(b"DOWNLOAD %d\n" % bytes)
        await self.writer.drain()
        start = time.time()
        reply = await self.reader.readexactly(bytes)
        elapsed = time.time() - start
        if bytes != len(reply):
            raise Exception("Response size does not match")
        return bytes / elapsed

    async def upload(self, bytes):
        """Measure the upload speed in bytes/s for the specified number of bytes"""
        command = b"UPLOAD %d\n" % bytes
        self.writer.write(command)
        chunk_size = len(self.random_bytes)
        start = time.time()
        missing = bytes - len(command) - 1
        while missing > 0:
            buf = self.random_bytes[:min(chunk_size, missing)]
            missing -= len(buf)
            self.writer.write(buf)
        self.writer.write(b"\n")
        reply = await self.reader.readline()
        elapsed = time.time() - start
        if not reply.decode('utf-8').startswith("OK %d" % bytes):
            raise Exception("Bad response or wrong size")
        return bytes / elapsed

async def main():
    if len(sys.argv) < 3:
        print("Usage: python client.py <host> <port>")
        exit()
    host = sys.argv[1]
    port = int(sys.argv[2])

    client = await Client.connect(host, port)

    hello = await client.hi()
    print(hello + "\n")

    print("Ping")
    for _ in range(4):
        ms = await client.ping()
        print("{:.1f} ms".format(ms))

    print("\nDownload")
    for _ in range(4):
        speed = await client.download(2_000_000)
        print("{:.3f} Mbit/s".format(speed * 8 / 1_000_000))

    print("\nUpload")
    for _ in range(4):
        speed = await client.upload(2_000_000)
        print("{:.3f} Mbit/s".format(speed * 8 / 1_000_000))

if __name__ == '__main__':
    try:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(main())
    except KeyboardInterrupt:
        pass
